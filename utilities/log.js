const time = require("./time");

exports.logCommand = (command, runner, userID, args = null) => {
    console.log("---------------------------------------");
    if(args[0] != null) {
        console.log(`Command ${command} with argument(s): ${args.join(", ")} was run by ${runner} with user ID ${userID}.`);
    } else {
        console.log(`Command ${command} was run by ${runner} with user ID ${userID}.`);
    }
    console.log(`Local Time: ${time.getCurrentTimeString()}`);
    console.log(`UTC Time: ${new Date(Date.now()).toUTCString()}`);
    console.log("---------------------------------------");
}

exports.logError = (error) => {
    console.log("---------------------------------------");
    console.log("\x1b[31mError!\x1b[0m");
    console.log(error);
    console.log(`Local Time: ${time.getCurrentTimeString()}`);
    console.log(`UTC Time: ${new Date(Date.now()).toUTCString()}`);
    console.log("---------------------------------------");
}

exports.logWarning = (warning) => {
    console.log("---------------------------------------");
    console.log("\x1b[33mWarning!\x1b[0m");
    console.log("Most warnings can be safely ignored.");
    console.log(warning);
    console.log(`Local Time: ${time.getCurrentTimeString()}`);
    console.log(`UTC Time: ${new Date(Date.now()).toUTCString()}`);
    console.log("---------------------------------------");
}