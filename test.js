if(require("fs").existsSync(".env")) {
    console.log("Everything is working!");
    process.exit(0);
} else {
    console.log("ERROR: .env file is missing!");
    process.exit(1);
}