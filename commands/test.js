const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    msg.reply("this is just a test you had the following arguments: " + args.join(", "));
    log.logCommand("test", msg.author.username, msg.author.id, args);
}