const Discord = require("discord.js");
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    let embed = new Discord.RichEmbed();
    embed.setTitle(":ping_pong: Pong! It took `" + bot.ping + "ms`. :ping_pong:");
    msg.channel.send({embed: embed});
    log.logCommand("ping", msg.author.username, msg.author.id, args);
}