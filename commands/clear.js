const Discord = require("discord.js");
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    let embed = new Discord.RichEmbed();
    if(!isNaN(args[0]) && args[0] >= 1 && args[0] <= 100) {
        if(msg.member.hasPermission("MANAGE_MESSAGES", false, true, true)) {
            if(args[0] <= 99) {
                msg.channel.fetchMessages({limit: parseInt(args[0])+1}).then((messages) => msg.channel.bulkDelete(messages, true).then((messages) => {
                    embed.setTitle("Successfully deleted " + messages.size + " messages.");
                    msg.channel.send({embed: embed});
                }));
            } else {
                msg.channel.fetchMessages({limit: parseInt(args[0])}).then((messages) => msg.channel.bulkDelete(messages, true).then((messages) => {
                    embed.setTitle("Successfully deleted " + messages.size + " messages.");
                    msg.channel.send({embed: embed});
                }));
            }
        }
    } else if(msg.mentions.members.first()) {
        if(msg.member.hasPermission("MANAGE_MESSAGES", false, true, true)) {
            msg.channel.fetchMessages().then((messages) => msg.channel.bulkDelete(messages.filter(m => m.author.id == msg.mentions.members.first().id), true).then((messages) => {
                embed.setTitle("Successfully deleted " + messages.size + " messages from user " + messages.first().author.tag + "(" + messages.first().author.id + ").");
                msg.channel.send({embed: embed});
                if(!msg.deleted) {
                    msg.delete();
                }
            }));
        }
    } else {
        embed.setTitle("Error");
        embed.setDescription("Command must either mention a user to clear the messages of, or include the number of messages to clear that is 1 or greater and equal to or less than 100.");
        msg.channel.send({embed: embed});
    }
    log.logCommand("clear", msg.author.username, msg.author.id, args);
}