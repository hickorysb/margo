const Discord = require('discord.js');
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    if(msg.mentions.users.first()) {
        let embed = new Discord.RichEmbed();
        if(msg.mentions.users.first().avatarURL){
            embed.setImage(msg.mentions.users.first().avatarURL);
        } else {
            embed.setDescription("User has no icon set.");
        }
        embed.setTitle(msg.mentions.users.first().tag);
        msg.channel.send({embed: embed});
    } else {
        let embed = new Discord.RichEmbed();
        if(msg.author.avatarURL){
            embed.setImage(msg.author.avatarURL);
        } else {
            embed.setDescription("User has no icon set.");
        }
        embed.setTitle(msg.author.tag);
        msg.channel.send({embed: embed});
        log.logCommand("avatar", msg.author.username, msg.author.id, args);
    }
}