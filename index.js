//Import packages
const dotenv = require('dotenv');
const Discord = require("discord.js");
const publicip = require("public-ip");
const time = require("./utilities/time");
//Setup & Initialization of packages
const bot = new Discord.Client();
let commands = {};
dotenv.config();

//Post login configuration
bot.on("ready", () => {
    console.log("-----------------------");
    console.log("  Margo is now online  ");
    console.log("-----------------------");
    (async () => {
        console.log("Useful Information");
        console.log(`Public IP: ${await publicip.v4()}`);
        console.log(`Local Time at Launch: ${time.getCurrentTimeString()}`);
        console.log(`UTC Time at Launch: ${new Date(Date.now()).toUTCString()}`);
        console.log(`Timezone Offset: ${new Date(Date.now()).getTimezoneOffset()/60} hours`);
        console.log(`Mode: ${process.env.ENVIRONMENT}`);
    })();
    require("fs").readdirSync(__dirname + "/" + "commands").forEach(function(file) {
        if(file.includes(".js")) {
            commands[file.replace(".js", "")] = require("./commands/" + file);
        }
    });
    bot.user.setPresence({game: {name: "with a hot potato"}, status: "online"});
});

//Catch messages and get command and arguments
bot.on("message", (msg) => {
    if(msg.guild.id == "645358354323210300" && process.env.ENVIRONMENT == "PRODUCTION") {
        return;
    } else if(msg.guild.id != "645358354323210300" && process.env.ENVIRONMENT == "DEVELOPMENT") {
        return;
    }
    if(msg.content.startsWith(process.env.PREFIX)) {
        let temp = msg.content.slice(process.env.PREFIX.length);
        let args = temp.split(" ");
        let command = args.shift();
        command = command.toLowerCase();
        if(command != "") {
            if(command in commands) {
                commands[command].run(bot, msg, args);
            } else {
                msg.reply("sorry, I can't find that one.");
            }
        }
    }
});

//Bot login
bot.login(process.env.DISCORD_BOT_TOKEN);