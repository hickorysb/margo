const Discord = require("discord.js");
const https = require("https");
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    const options = {
        hostname: "icanhazdadjoke.com",
        port: 443,
        path: "/",
        method: "GET",
        headers: {
            "Accept": "application/json"
        }
    }

    https.request(options, (resp) => {
        let data = "";

        resp.on("data", (chunk) => {
            data += chunk;
        });
        resp.on("end", () => {
            let jokeJSON = JSON.parse(data);

            let embed = new Discord.RichEmbed();
            if("joke" in jokeJSON) {
                embed.setTitle(jokeJSON.joke);
                msg.channel.send({embed: embed});
                log.logCommand("dadjoke", msg.author.username, msg.author.id, args);
            } else {
                log.logError("Invalid response.");
            }
        });
    }).on("error", (error) => {
        log.logError(error.message);
    }).end();
}
