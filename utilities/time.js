exports.getCurrentTimeString = () => {
    let day = "";
    switch(new Date(Date.now()).getDay()) {
        case 0:
            day = "Sun";
            break;
        case 1:
            day = "Mon";
            break;
        case 2:
            day = "Tue";
            break;
        case 3:
            day = "Wed";
            break;
        case 4:
            day = "Thu";
            break;
        case 5:
            day = "Fri";
            break;
        case 6:
            day = "Sat";
            break;
    }
    let date = "";
    if (new Date(Date.now()).getDate() > 9) {
        date = new Date(Date.now()).getDate().toString();
    } else {
        date = "0" + new Date(Date.now()).getDate().toString();
    }
    return `${day}, ${date} ${new Date(Date.now()).toLocaleString("en-US", {year: 'numeric', month: 'short'})} ${new Date(Date.now()).toLocaleString("en-US", {hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false, timeZoneName: "short"})}`;
}