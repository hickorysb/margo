const Discord = require("discord.js");
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    let embed = new Discord.RichEmbed();
    if(args[0] == null || args[0] < 0) {
        args[0] = 1;
    } else if(args[0] > 2) {
        args[0] = 1;
    }
    switch(args[0]) { 
        case 1:
            embed.setTitle("Help");
            embed.setFooter("Prefix: " + process.env.PREFIX + " | Page 1/2");
            embed.setDescription("Arguments for commands that are enclosed in `[]` are optional, arguments enclosed in `{}` are mandatory. Not including a mandatory argument will result in an error.");
            embed.addField("Help [page]", "Displays this help message.");
            embed.addField("Ping", "Displays the ping for the bot.");
            embed.addField("Test", "This command is just meant to test whether the bot is properly working.");
            embed.addField("Clear {count} or Clear {@user}", "Clears up to 100 messages at a time. Messages must be 14 days old or newer. Count must be between 1 and 100 inclusive.");
            embed.addField("Chocolate", "A command that an idiot friend of mine demanded I add. It just sends an embed with the title `Chocolate` with a link to an essay on why chocolate is good for you.");
            embed.addField("DadJoke", "Sends an embed that has a Dad joke. Provided by the `icanhazdadjoke.com` API.");
            embed.addField("Meme [subreddit]", "Sends a meme in chat from a list of predefined subreddits. The argument must also be in the list. Insert `help` as the subreddit for a list of available subreddits.");
            embed.addField("Avatar [@user]", "Returns your avatar or the avatar of the mentioned user.");
            break;
        case 2:
            embed.setTitle("Help");
            embed.setFooter("Prefix: " + process.env.PREFIX + " | Page 2/2");
            embed.setDescription("Arguments for commands that are enclosed in `[]` are optional, arguments enclosed in `{}` are mandatory. Not including a mandatory argument will result in an error.");
            embed.addField();
            break;
    }
    msg.channel.send({embed: embed});
    log.logCommand("help", msg.author.username, msg.author.id, args);
}