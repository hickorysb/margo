const Discord = require("discord.js");
const https = require("https");
const log = require("../utilities/log");

exports.run = (bot, msg, args) => {
    let options = {};
    let embed = new Discord.RichEmbed();
    switch(args[0].toLowerCase()) {
        case "help":
            embed.setTitle("List of available subreddits.");
            embed.setDescription("programmerhumor");
            msg.channel.send({embed: embed});
            return;
        case "programmerhumor":
            options = {
                hostname: "meme-api.herokuapp.com",
                port: 443,
                path: "/gimme/programmerhumor",
                method: "GET"
            }
            break;
        default:
            options = {
                hostname: "meme-api.herokuapp.com",
                port: 443,
                path: "/gimme",
                method: "GET"
            }
            break;
    }

    https.request(options, (resp) => {
        let data = "";

        resp.on("data", (chunk) => {
            data += chunk;
        });
        resp.on("end", () => {
            let memeJSON = JSON.parse(data);

            if("url" in memeJSON) {
                embed.setTitle(memeJSON.title);
                embed.setFooter(memeJSON.subreddit);
                embed.setImage(memeJSON.url);
                embed.setURL(memeJSON.postLink);
                msg.channel.send({embed: embed});
                log.logCommand("memeJSON", msg.author.username, msg.author.id, args);
            } else {
                log.logError("Invalid response.");
            }
        });
    }).on("error", (error) => {
        log.logError(error.message);
    }).end();
}
